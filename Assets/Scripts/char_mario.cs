using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class char_mario : elemento_movil {

	private int puntuacion;		//La puntuación que obtenemos a lo largo del nivel, no la total en todo el juego.

	private bool finalizado = false;	//Para controlar si hemos tocado la meta, e iniciar la secuencia adecuada.

	//Movimiento
	private bool mover_izq, mover_drc, saltar, mover;
	//Potencia de salto de Mario
	public float saltoVel;

	//Camara que observa a Mario para reproducir sonidos en determinados eventos
	public Camera camara;
	//Sonidos que puede producir Mario
	public AudioClip[] audioArray = new AudioClip[3];

	//Constructor para el Mario de cada fase(SIN USAR)
	public char_mario(int poder_act) {
		this.puntuacion = 0;			//Empezamos con 0 de puntuación del personaje, en todas las fases.
		this.poder = poder_act;		//En un principio Mario comienza como pequeño, pero en fases contiguas se mantendría su estado
	}

	/////////////////////////////////////
	////////////Raycasting///////////////
	/////////////////////////////////////
	protected override Vector2 comprRaycastingMuros(Vector2 pos, float direccion){
		if (this.poder >= 2){
			estatura = 1f;
		}else{
			estatura = 0.5f;
		}
		//Esto nos da una posicion de origen para los raycast, la parte de arriba para colisiones con bloques, la del medio para contacto enemigos y la parte de abajo para aplastar enemigos
		Vector2 marioTop = new Vector2(pos.x + direccion * .4f, pos.y + estatura - 0.2f);		//Cabeza
		Vector2 marioMid = new Vector2(pos.x + direccion * .4f, pos.y);							//Mitad del cuerpo
		Vector2 marioBot = new Vector2(pos.x + direccion * .4f, pos.y - estatura + 0.2f);		//Pies

		//Esto nos da la posicion de los muros existentes
		RaycastHit2D muroTop = Physics2D.Raycast(marioTop, new Vector2(direccion, 0), velocidad.x * Time.deltaTime, muros);
		RaycastHit2D muroMid = Physics2D.Raycast(marioMid, new Vector2(direccion, 0), velocidad.x * Time.deltaTime, muros);
		RaycastHit2D muroBot = Physics2D.Raycast(marioBot, new Vector2(direccion, 0), velocidad.x * Time.deltaTime, muros);
		if (muroTop.collider != null || muroMid.collider != null || muroBot.collider != null){
			//Lo inicializamos a un valor cualquiera, irrelevante
			RaycastHit2D rayContacta = muroTop;
			//No usamos un switch porque se quedaria con la primera opcion aparente y no tendría el resto en cuenta secuencialmente
			if (muroTop){
				rayContacta = muroTop;
			}else if(muroMid){
				rayContacta = muroMid;
			}else if(muroBot){
				rayContacta = muroBot;
			}
			//Si tocamos la bandera
			if(rayContacta.collider.tag == "meta"){
				rayContacta.collider.GetComponent<Collider2D>().enabled = false;
				iniVictor();		
			}
			//Y finalmente si colisiona con algun muro, detenemos al personaje.
			pos.x -= this.velocidad.x * Time.deltaTime * direccion;
		}
		return pos;
	}

	protected override Vector2 comprRaycastingSuelo(Vector2 pos){
 
 		if (this.poder >= 2){
			estatura = 1f;
		}else{
			estatura = 0.5f;
		}

		//Posiciones de mario respecto al suelo
		Vector2 marioIzq = new Vector2(pos.x - 0.5f + 0.2f, pos.y - estatura);		
		Vector2 marioCen = new Vector2(pos.x, pos.y - estatura);			
		Vector2 marioDrc = new Vector2(pos.x + 0.5f - 0.2f, pos.y - estatura); 
	
		//Posiciones del suelo respecto a Mario
		RaycastHit2D sueloIzq = Physics2D.Raycast(marioIzq, new Vector2(0,-1), velocidad.y * Time.deltaTime, suelo);
		RaycastHit2D sueloCen = Physics2D.Raycast(marioCen, new Vector2(0,-1), velocidad.y * Time.deltaTime, suelo);
		RaycastHit2D sueloDrc = Physics2D.Raycast(marioDrc, new Vector2(0,-1), velocidad.y * Time.deltaTime, suelo);
		//Finalmente devolvemos si toca el suelo
		if (sueloIzq.collider != null || sueloCen.collider != null || sueloDrc.collider != null) {
			//Siempre tenemos en cuenta de entrada el suelo que tenemos a continuacion
			RaycastHit2D rayContacta = sueloDrc;
			//No usamos un switch porque se quedaria con la primera opcion aparente y no tendría el resto en cuenta secuencialmente
			if (sueloIzq){
				rayContacta = sueloIzq;
			}else if(sueloCen){
				rayContacta = sueloCen;
			}else if(sueloDrc){
				rayContacta = sueloDrc;
			}
			//Si pisamos a un enemigo
			if (rayContacta.collider.tag == "Enemy")
				rayContacta.collider.GetComponent<elemento_vivo>().aplastado();
			//Si nos caemos del escenario
			if (rayContacta.collider.tag == "caida_muerte")
				caerAlVacio();
			//Variable de contacto
			this.tocaSuelo = true;
			//Animacion de quieto(a pesar de la inercia que pueda haber)
			estado = estado_index.quieto;
			//La velocidad vertical al tocar el suelo es 0
			velocidad.y = 0;
			//Vemos el sitio donde vamos a caer y comprobamos su centro y tamaño para ver si caemos en el o si seguimos cayendo
			pos.y = rayContacta.collider.bounds.center.y + rayContacta.collider.bounds.size.y / 2 + estatura;

		} else {
			if (estado != estado_index.salto)
				caer();
		}

		return pos;

	}

	Vector2 comprRaycastingTecho(Vector2 pos){

		if (this.poder >= 2){
			estatura = 1f;
		}else{
			estatura = 0.5f;
		}

		//Posiciones de mario respecto al suelo
		Vector2 marioOri = new Vector2(pos.x - 0.5f + 0.2f, pos.y + estatura);		
		Vector2 marioFro = new Vector2(pos.x, pos.y + estatura);			
		Vector2 marioOcc = new Vector2(pos.x + 0.5f - 0.2f, pos.y + estatura); 
	
		//Posiciones del techo respecto a Mario
		RaycastHit2D techoOri = Physics2D.Raycast(marioOri, new Vector2(0,1), velocidad.y * Time.deltaTime, suelo);
		RaycastHit2D techoFro = Physics2D.Raycast(marioFro, new Vector2(0,1), velocidad.y * Time.deltaTime, suelo);
		RaycastHit2D techoOcc = Physics2D.Raycast(marioOcc, new Vector2(0,1), velocidad.y * Time.deltaTime, suelo);
		//Finalmente devolvemos si toca el suelo
		if (techoOri.collider != null || techoFro.collider != null || techoOcc.collider != null) {
			//Siempre tenemos en cuenta de entrada el suelo que tenemos a continuacion
			RaycastHit2D rayContacta = techoOri;
			//No usamos un switch porque se quedaria con la primera opcion aparente y no tendría el resto en cuenta secuencialmente
			if (techoOri){
				rayContacta = techoOri;
			}else if(techoFro){
				rayContacta = techoFro;
			}else if(techoOcc){
				rayContacta = techoOcc;
			}
		
			//Mantenemos la posicion sin atravesar el bloque
			pos.y = rayContacta.collider.bounds.center.y - rayContacta.collider.bounds.size.y / 2 - estatura;
			//Comenzamos a caer tras tocar el bloque
			caer();
		}

		return pos;

	}
	
	//Se llama cuando se toca un power-up(flor o champiñon).
	void adquirir_powerup(){
		if(this.poder==3){				//Si ya estamos en el estado nº3(Mario con canicas de fuego), coger otra flor/powerup nos da puntos, ya que no necesitamos más powerups.
			this.puntuacion += 300;
		}else{
			this.poder++;				//Si no, subimos al siguiente estado.
			//crecer();
		}
	}	
/*
	void crecer(){
		if (this.poder >= 2){
			//Animaciones de mario grande
			GetComponent<Animator>().SetBool("isTiny",false);			
			Vector2 aux = new Vector2(0,0);
			//Offset
			aux.x = 0f;
			aux.y = 0.02f;
			GetComponent<BoxCollider2D>().offset = aux;
			//Reutilizamos el vector para el tamaño de la hitbox
			aux.x = 1f;
			aux.y = 1.97f;
			GetComponent<BoxCollider2D>().size = aux;
		}
	}
*/

	void encoger(){
		if (this.poder == 1){
			//Audio a reproducir
			GetComponent<AudioSource>().clip = audioArray[1];
			GetComponent<AudioSource>().Play();
			//Cambiamos las animaciones
			GetComponent<Animator>().SetBool("isTiny",true);			
			Vector2 aux = new Vector2(0,0);
			//Offset
			aux.x = 0;
			aux.y = 0;
			GetComponent<BoxCollider2D>().offset = aux;
			//Reutilizamos el vector para el tamaño de la hitbox
			aux.x = 1;
			aux.y = 1;
			GetComponent<BoxCollider2D>().size = aux;
		}
	}

	//Se llama cuando se toca a un enemigo/elemento dañino no mortal del escenario
	//Es pública ya que la llama el enemigo
	public void recibirGolpe(){
		this.poder--;
		//Comprobamos si hay que cambiar la hitbox
		if(this.poder <= 0){
			dannyBoi();
		}else{
			encoger();
		}
	}

	//Morir al caer bajo el escenario/lava/agua no nadable
	protected override void caerAlVacio(){
		dannyBoi();
	}

	//Marcha fúnebre
	void dannyBoi(){
		//Detenemos el audio de la camara
		camara.GetComponent<AudioSource>().Stop();
		//Clip de morir
		camara.GetComponent<AudioSource>().clip = audioArray[2];
		GetComponent<AudioSource>().Play();
		StartCoroutine(morir());	
	}


	//Secuencia de muerte
	protected override IEnumerator morir(){
		//Detenemos a Mario en su sitio y lo ponemos como muerto
		this.poder = 0;
		estado = estado_index.muerto;
		this.velocidad.x = 0;
		//Aniamcion y musica
		yield return new WaitForSeconds(3f);
		SceneManager.LoadScene(1);			
	}

	//Iniciar la secuencia de victoria
	void iniVictor(){
		//Clip de victoria
		camara.GetComponent<AudioSource>().Stop();
		GetComponent<AudioSource>().clip = audioArray[3];
		GetComponent<AudioSource>().Play();

		StartCoroutine(victoria());
	}

	IEnumerator victoria(){
		//Nuestro estado de victoria se vuelve verdadero
		this.finalizado = true;
		//Caemos al suelo y comenzamos a andar
		this.velocidad.y = 0;
		this.velocidad.x = 2;
		//Sonido de victoria
		yield return new WaitForSeconds(8f);
		SceneManager.LoadScene(1);
	}

	/////////////////////////////////////
	////Movimiento, Input y Animacion////
	/////////////////////////////////////

	//Comprobacion de botones
	void pulsarBoton(){	
		bool input_drc = Input.GetKey(KeyCode.RightArrow); 
		bool input_izq = Input.GetKey(KeyCode.LeftArrow);
		bool input_salto = Input.GetKey(KeyCode.Space);

		mover = input_drc || input_izq;
		mover_izq = !input_drc && input_izq;
		mover_drc = input_drc && !input_izq;
		saltar = input_salto;
		if(finalizado){
			mover = true;
			mover_drc =true;
			mover_izq = false;
			saltar = false;
		}
	}

	protected override void movimiento(){
		
		Vector2 pos = transform.localPosition;
		Vector2 scale = transform.localScale;	
		if(estado!=estado_index.muerto){		
			if (mover){
				if(mover_drc){
					scale.x = 1;
					pos.x += velocidad.x * Time.deltaTime;
				}
				if(mover_izq){
					scale.x = -1;
					pos.x -= velocidad.x * Time.deltaTime;
				}
				pos = comprRaycastingMuros(pos, scale.x);
			}

			if (saltar && estado != estado_index.salto){
				//Sonido de salto
				GetComponent<AudioSource>().clip = audioArray[0];
				GetComponent<AudioSource>().Play();
				//Si no estabamos saltando, comenzamos a saltar
				this.estado = estado_index.salto;
				//La velocidad vertical que alcanzamos es igual a la velocidad de salto que determinemos
				velocidad = new Vector2(velocidad.x, saltoVel);
			}
			//Si ya estamos en el aire(al inicio o nada mas saltar)
			if (estado == estado_index.salto){
				//Primero subimos gracias a la velocidad que nos da el vector anterior
				pos.y += velocidad.y * Time.deltaTime;
				//Cuando la gravedad nos supera empezamos a caer
				velocidad.y -= gravedad * Time.deltaTime;
			}

			//Esto significa que mario ha pasado el punto en que ya le afecta la gravedad
			if (velocidad.y <= 0)
				pos = comprRaycastingSuelo(pos);
			
			if (velocidad.y >= 0)
				pos = comprRaycastingTecho(pos);

			transform.localPosition = pos;
			transform.localScale = scale;
		}
	}

	void cambioAnimacion(){
		if (this.poder >=2){
			GetComponent<Animator>().SetBool("isTiny",false);
		}else{
			GetComponent<Animator>().SetBool("isTiny",true);
		}
		if(tocaSuelo && !mover){
			GetComponent<Animator>().SetBool("isJumping",false);
			GetComponent<Animator>().SetBool("isRunning",false);
		}
		if(tocaSuelo && mover){
			GetComponent<Animator>().SetBool("isJumping",false);
			GetComponent<Animator>().SetBool("isRunning",true);
		}
		if (estado==estado_index.salto){
			GetComponent<Animator>().SetBool("isJumping",true);
			GetComponent<Animator>().SetBool("isRunning",false);
		}		
	}

	//////////////////////
	//Funciones de Unity//
	//////////////////////
	void Start(){
		estado = estado_index.quieto;
		this.poder = 2;
		caer();
	}

	//No empleamos un Case porque de usarlo solo se ejecutaría una de las acciones disponibles, sin permitirnos, por ejemplo, saltar y movernos a la vez.
	void Update(){
		pulsarBoton();
		movimiento();
		cambioAnimacion();
	}
}
