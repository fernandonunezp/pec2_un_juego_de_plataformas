using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class elemento_movil : MonoBehaviour {

	//Valor que indica la magnitud de la gravedad que afecto al elemento
	public float gravedad;
	//Booleano que indica si el elemento está en contacto con el suelo
	protected bool tocaSuelo;
	//Estatura del elemento, empleada en el calculo de colisiones
	protected float estatura;
	//Variable de control de altura para detección de colisiones
	protected int poder;

	//Estados en los que se puede encontrar el elemento
	protected enum estado_index{
		quieto,
		salto,
		moviendo,
		muerto
	}
	//Estado empleado para determinar animaciones, fisicas que afectan, etc.
	protected estado_index estado;

	//Vector para el movimiento
	public Vector2 velocidad;
	//Referencias de muros con los que colisionar(no atravesar)
	public LayerMask muros;
	//Referencia a la capa del suelo que tendrán en cuentan las colisiones
	public LayerMask suelo;


	protected void caer(){
		//La velocidad vertical es 0 de modo que le afecte la gravedad al elemento
		velocidad.y = 0;
		//La gravedad(y respectivas colisiones) las comprobamos en el estado de vuelo, ya que si no las comprobaciones de colision sobreacargarían el PC
		estado = estado_index.salto;
		//Estamos en el aire
		tocaSuelo = false;
	}
	protected abstract void caerAlVacio();		//Que pasa al caer al vacio
	protected abstract void movimiento();		//COmo se mueve
	protected abstract IEnumerator morir();		//Que proceso sigue al morir/destruirse
	protected abstract Vector2 comprRaycastingSuelo(Vector2 pos); 	//Comprobacion de colisiones con elementos que toquemos con la parte baja del collider
	protected abstract Vector2 comprRaycastingMuros(Vector2 pos, float direccion); 	//Comprobacion de colisiones con elementos que toquemos horizontalmente

}
