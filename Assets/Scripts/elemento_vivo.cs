using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Clase que define el movimiento de un enemigo o de una seta power-up
public class elemento_vivo : elemento_movil {

	//////FUNCIONES RAYCASTING//////
	protected override Vector2 comprRaycastingSuelo(Vector2 pos){
 
 		if (this.poder >= 2){
			estatura = 1f;
		}else{
			estatura = 0.5f;
		}
		//Posiciones de mario respecto al suelo
		Vector2 enemigoIzq = new Vector2(pos.x - 0.5f + 0.2f, pos.y - estatura);		
		Vector2 enemigoCen = new Vector2(pos.x, pos.y - estatura);			
		Vector2 enemigoDrc = new Vector2(pos.x + 0.5f - 0.2f, pos.y - estatura); 
		//Posiciones del suelo respecto a Mario
		RaycastHit2D sueloIzq = Physics2D.Raycast(enemigoIzq, new Vector2(0,-1), velocidad.y * Time.deltaTime, suelo);
		RaycastHit2D sueloCen = Physics2D.Raycast(enemigoCen, new Vector2(0,-1), velocidad.y * Time.deltaTime, suelo);
		RaycastHit2D sueloDrc = Physics2D.Raycast(enemigoDrc, new Vector2(0,-1), velocidad.y * Time.deltaTime, suelo);
		//Finalmente devolvemos si toca el suelo
		if (sueloIzq.collider != null || sueloCen.collider != null || sueloDrc.collider != null) {
			//Siempre tenemos en cuenta de entrada el suelo que tenemos a continuacion
			RaycastHit2D rayContacta = sueloDrc;
			//No usamos un switch porque se quedaria con la primera opcion aparente y no tendría el resto en cuenta secuencialmente
			if (sueloIzq){
				rayContacta = sueloIzq;
			}else if(sueloCen){
				rayContacta = sueloCen;
			}else if(sueloDrc){
				rayContacta = sueloDrc;
			}
			if (rayContacta.collider.tag == "caida_muerte")
				caerAlVacio();
			//Variable de contacto
			this.tocaSuelo = true;
			//Animacion de quieto(Para los enemigos quieto es moverse, para Mario no. Eso se ajusta en la funcion de movimiento)
			this.estado = estado_index.quieto;
			//La velocidad vertical al tocar el suelo es nula
			velocidad.y = 0;
			//Ajustamos la posicion vertival respecto a nuestra hitbox
			pos.y = rayContacta.collider.bounds.center.y + rayContacta.collider.bounds.size.y / 2 + estatura;

		} else {
			if (estado != estado_index.salto)
				caer();
		}
		return pos;
	}


	protected override Vector2 comprRaycastingMuros(Vector2 pos, float direccion){
		if (this.poder >= 2){
			estatura = 1f;
		}else{
			estatura = 0.5f;
		}
		//Esto nos da una posicion de origen para los raycast, la parte de arriba para colisiones con bloques, la del medio para contacto enemigos y la parte de abajo para aplastar enemigos
		Vector2 enemigoTop = new Vector2(pos.x + direccion * .4f, pos.y + estatura - 0.2f);		//Cabeza
		Vector2 enemigoMid = new Vector2(pos.x + direccion * .4f, pos.y);						//Mitad del cuerpo
		Vector2 enemigoBot = new Vector2(pos.x + direccion * .4f, pos.y - estatura + 0.2f);		//Pies
		//Esto nos da la posicion de los muros existentes
		RaycastHit2D muroTop = Physics2D.Raycast(enemigoTop, new Vector2(direccion, 0), velocidad.x * Time.deltaTime, muros);
		RaycastHit2D muroMid = Physics2D.Raycast(enemigoMid, new Vector2(direccion, 0), velocidad.x * Time.deltaTime, muros);
		RaycastHit2D muroBot = Physics2D.Raycast(enemigoBot, new Vector2(direccion, 0), velocidad.x * Time.deltaTime, muros);

		if (muroTop.collider != null || muroMid.collider != null || muroBot.collider != null){
			//Lo inicializamos a un valor cualquiera, irrelevante
			RaycastHit2D rayContacta = muroTop;
			//No usamos un switch porque se quedaria con la primera opcion aparente y no tendría el resto en cuenta secuencialmente
			if (muroTop){
				rayContacta = muroTop;
			}else if(muroMid){
				rayContacta = muroMid;
			}else if(muroBot){
				rayContacta = muroBot;
			}
			if(rayContacta.collider.tag == "Player")
				rayContacta.collider.GetComponent<char_mario>().recibirGolpe();

			//Y finalmente si colisiona con algun muro, detenemos al personaje.
			Vector3 trans = transform.localScale;
			trans.x = -trans.x;
			transform.localScale = trans;
		}

		return pos;
	}

	//////////////////////////////////////////////
	////////////Funciones propias/////////////////
	//////////////////////////////////////////////

	//Funcion que se llama cuando Mario nos pisa, es pública porque la llama Mario
	public void aplastado(){
		this.poder = 0;
		GetComponent<AudioSource>().Play();
		StartCoroutine(morir());
	}

	//Cuando mario cae por un agujero
	protected override void caerAlVacio(){
		Destroy(this.gameObject);
	}

	protected override IEnumerator morir(){
		if(this.poder <= 0){
			//Si pisan al enemigo se muere
			estado = estado_index.muerto;
			//Le damos la aimacoin de muerto
			GetComponent<Animator>().SetBool("isCrushed",true);
			//El enmigo pierde su colisionador(pero no se deshabilita aun, eso lo hace morir())
			GetComponent<Collider2D>().enabled = false;
			yield return new WaitForSeconds(1f);
	 		Destroy(this.gameObject);
		}
	}

	protected override void movimiento(){

		if(estado != estado_index.muerto){
			Vector2 pos = transform.localPosition;
			Vector2 scale = transform.localScale;

			if(estado == estado_index.quieto){
				if(scale.x < 0){	
					pos.x -= velocidad.x * Time.deltaTime;	
				}else{
					pos.x += velocidad.x * Time.deltaTime;	
				}			
			}

			if (estado == estado_index.salto){
				//Primero subimos gracias a la velocidad que nos da el vector anterior
				pos.y += velocidad.y * Time.deltaTime;
				//Cuando la gravedad nos supera empezamos a caer
				velocidad.y -= gravedad * Time.deltaTime;
			}

			//Esto significa que mario ha pasado el punto en que ya le afecta la gravedad
			if (velocidad.y <= 0){
				pos = comprRaycastingSuelo(pos);
				
			}
			pos = comprRaycastingMuros(pos, scale.x);	
			
			transform.localPosition = pos;
		}
	}

	//Unity activa automaticamente el elemento cuando lo coge la cámara(LA PANACEA)
	void OnBecameVisible(){
		this.enabled = true;
	}

	// Use this for initialization
	void Start () {
		estado = estado_index.salto;
		//No cargamos a los enemigos al comienzo porque si no los que aperezcan mas adelante se caen al vacío
		this.poder = 1;
		this.enabled = false;
		//Que automaticamente se ajuste con el suelo
		caer();
	}
	
	// Update is called once per frame
	void Update () {
		movimiento();
	}

}