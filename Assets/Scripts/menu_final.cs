﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class menu_final : MonoBehaviour {

	public RectTransform punteroA;
	public RectTransform punteroB;

	bool val = false;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.RightArrow)){
			val = true;
			punteroA.GetComponent<Image>().enabled = true;
			punteroB.GetComponent<Image>().enabled = false;
		} 
		if (Input.GetKey(KeyCode.LeftArrow)){
			val = false;
			punteroA.GetComponent<Image>().enabled = false;
			punteroB.GetComponent<Image>().enabled = true;

		}
		if (Input.GetKey(KeyCode.Space)){
			if (!val){
				SceneManager.LoadScene(0);
			}else{
				Application.Quit();
			}
		}
		 
	}
}
